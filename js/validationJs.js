const surveyForm = document.getElementById("surveyForm"),
    zip = document.getElementById("zip"),
    city = document.getElementById("city"),
    state = document.getElementById("state");
function btnSecondNext() {
    debugger; 
    var e = document.getElementsByClassName("needs-validation");
    // Array.prototype.filter.call(e, function (e) {
    //     "" == zip.value ? ($(".main-content").css({ height: "130vh" }), $(".second-section-optional-field").show(), e.classList.add("was-validated")) : ($(".form-section-three").show(), $(".form-section-two").hide());
    // });
    Array.prototype.filter.call(e, function (e) {
        "" == zip.value ? ($(".main-content").css({ height: "130vh" }), $(".second-section-optional-field").show(), e.classList.add("was-validated")) : ($(".form-section-four").show(), $(".form-section-two").hide());
    });
  
}
const setZipError = (e, t) => {
        const r = e.parentElement;
        (r.querySelector(".zipError").innerText = t), r.classList.add("zipError"), r.classList.remove("success");
    },
    setZipSuccess = (e) => {
        const t = e.parentElement;
        (t.querySelector(".zipError").innerText = ""), t.classList.add("success"), t.classList.remove("zipError");
    },
    setCityError = (e, t) => {
        e.parentElement.querySelector(".cityError").innerText = t;
    },
    setCitySuccess = (e) => {
        const t = e.parentElement;
        (t.querySelector(".cityError").innerText = ""), t.classList.add("success"), t.classList.remove("cityError");
    },
    setStateError = (e, t) => {
        e.parentElement.querySelector(".stateError").innerText = t;
    };
function surveySectionTwoNext() {
    let e = zip.value;
    document.getElementById("SectionNineInputZip");
    if ("" != e)
        if (e.length >= 5) $(".survey-section-2").hide(), $(".survey-section-3").show();
        else {
            setZipError(zip, "Please enter minimum 5 Digit. (i.e. 90210)");
            let e = document.getElementById("optionalField");
            "block" === e.style.display ? (e.style.display = "none") : (e.style.display = "block"),
                (city.style.borderColor = "red"),
                setCityError(city, "Please complete this field."),
                (state.style.borderColor = "red"),
                setStateError(state, "Please complete this field.");
        }
    else {
        setZipError(zip, "Please enter a valid ZIP Code. (i.e. 90210)");
        let e = document.getElementById("optionalField");
        "block" === e.style.display ? (e.style.display = "none") : (e.style.display = "block"),
            (city.style.borderColor = "red"),
            setCityError(city, "Please complete this field."),
            (state.style.borderColor = "red"),
            setStateError(state, "Please complete this field.");
    }
}
const form = document.getElementById("emailForm"),
    email = document.getElementById("Email");
function surveySectionEightNext() {
    if ("" == email.value) (email.style.borderColor = "red"), validateInputs();
    else {
        let e = navigator.appCodeName,
            t = navigator.product,
            r = navigator.appVersion;
        (document.getElementById("userAgent").value = e + " " + t + " " + r), $(".survey-section-8").hide(), $(".survey-section-9").show();
    }
}
const setError = (e, t) => {
        const r = e.parentElement;
        (r.querySelector(".error").innerText = t), r.classList.add("error"), r.classList.remove("success");
    },
    setSuccess = (e) => {
        const t = e.parentElement;
        (t.querySelector(".error").innerText = ""), t.classList.add("success"), t.classList.remove("error");
    },
    isValidEmail = (e) => /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(String(e).toLowerCase()),
    validateInputs = () => {
        const e = email.value.trim();
        "" === e ? setError(email, "Email is required") : isValidEmail(e) ? setSuccess(email) : setError(email, "Provide a valid email address");
    };
let streetAddress = document.getElementById("inputStreet"),
    SectionNineInputZip = document.getElementById("SectionNineInputZip"),
    inpuTCity = document.getElementById("inpuTCity"),
    inputState = document.getElementById("inputState");
function surveyNineNextButton() {
    let e = streetAddress.value;
    document.getElementById("inputState");
    if ("" == e) {
        let e = document.getElementById("sectionNineOptionalFiled"),
            t = document.getElementById("ChangeBtn");
        !(function () {
            const e = streetAddress.parentElement;
            e.querySelector(".streetAddressErrorMessage").innerText = "Please complete this field.";
        })(),
            (streetAddress.style.borderColor = "red"),
            (SectionNineInputZip.style.borderColor = "red"),
            (inpuTCity.style.borderColor = "red"),
            (inputState.style.borderColor = "red"),
            (t.style.display = "none"),
            "block" === e.style.display ? (e.style.display = "none") : (e.style.display = "block");
    } else {
        const e = streetAddress.value.split(","),
            t = e[0],
            r = e[1],
            s = e[2];
        $(streetAddress).val(t), $("#state option:nth-child(1)").val(s).attr("selected", "selected"), city.setAttribute("value", r), $(".survey-section-9").hide(), $(".survey-section-10").show();
    }
}
let firstName = document.getElementById("inputFirstName"),
    lastName = document.getElementById("inputLastName");
function surveyTenNext() {
    let e = firstName.value,
        t = lastName.value;
    "" != e && "" != t ? ($(".survey-section-10").hide(), $(".survey-section-11").show()) : ((firstName.style.borderColor = "red"), (lastName.style.borderColor = "red"), validateMessageNameSection());
}
const setErrorFirstName = (e, t) => {
        e.parentElement.querySelector(".streetAddressErrorMessage").innerText = t;
    },
    setErrorLastName = (e, t) => {
        e.parentElement.querySelector(".streetAddressErrorMessage").innerText = t;
    };
function validateMessageNameSection() {
    var e;
    (e = "First name is required."), (firstName.parentElement.querySelector(".streetAddressErrorMessage").innerText = e), setErrorLastName(lastName, "Last name is required.");
}
